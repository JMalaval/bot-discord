const YoutubeStream = require('youtube-audio-stream');
import Discord = require('discord.js');

export default class Play {

    static match(message : Discord.Message) {
        return message.content.startsWith('!play')
    }

    
    static action(message : any) {

        let channelWanted = message.content.split(' ')[1];
        let urlWanted = message.content.split(' ')[2];
        let guildsChannel = message.guild.channels
        .filter((channel: any) =>{ return (channel.name === channelWanted && channel.type === 'voice')})
        .first();

        message.delete();
        guildsChannel
        .join()
        .then((connection: any) => {
            const stream = YoutubeStream(urlWanted);
            connection.playStream(stream).on('end', () => {
                connection.disconnect()
            });

        })
        .catch((error: any) => console.warn(error))
    }

}