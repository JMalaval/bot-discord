
import Discord = require('discord.js');

export default class Random {

    static match(message: Discord.Message) {
        return message.content.startsWith('!random')
    }

    static action(message: Discord.Message) {
        const max = message.content.split(' ')[1] ? Number(message.content.split(' ')[1]) : 6; 
        const random = Math.floor(Math.random() * Math.floor(max));
        message.delete();
        message.channel.send(random);


    }

}