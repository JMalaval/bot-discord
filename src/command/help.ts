
import Discord = require('discord.js');

const helps = [
    "- !play <voiceChannelName> <youtubeUrl>          Permet de faire jouer au bot l'audio d'une vidéo youtube par le bot . Indiquer le nom du channel vocal dans lequel le bot doit se connecter et jouer puis l'url de la vidéo youtube .",
    "- !random <maxNumber>         Permet de sortir un chiffre random de 0 à 6 par défault . Un chiffre peut être précisé pour le chiffre max du random . "  
]


export default class Help {

    static match(message: Discord.Message) {
        return message.content.startsWith('!help')
    }

    static action(message: Discord.Message) {
        helps.forEach((help) => {
            message.channel.send(help)
            message.delete();
        })
    }

}