import Discord = require('discord.js');
import Help from './command/help';
import Play from './command/play';
import Random from './command/random';
const client = new Discord.Client();



client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', (message: Discord.Message) => {
    if (message.content.startsWith('!')) {
        let commands = message.content.split(' ')[0].replace('!', '');
        if(Help.match(message)) Help.action(message);
        if(Play.match(message)) Play.action(message);
        if(Random.match(message)) Random.action(message);
        console.log("- Commande " + message.content.split(' ')[0] + " utilisée par " + message.author.username + " le " + new Date() )
    }
});

client.on('messageUpdate', (oldMember, newMember) => {
    console.log('Presence:', newMember)
  
    
})



client.login('NTY1OTQxNzE1ODExMzAzNDI0.XK9xNQ.nDYRjmzgDGiJ-HSQUcbDFwCBdQM')