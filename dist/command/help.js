"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helps = [
    "- !play <voiceChannelName> <youtubeUrl>          Permet de faire jouer au bot l'audio d'une vidéo youtube par le bot . Indiquer le nom du channel vocal dans lequel le bot doit se connecter et jouer puis l'url de la vidéo youtube .",
    "- !random <maxNumber>         Permet de sortir un chiffre random de 0 à 6 par défault . Un chiffre peut être précisé pour le chiffre max du random . "
];
class Help {
    static match(message) {
        return message.content.startsWith('!help');
    }
    static action(message) {
        helps.forEach((help) => {
            message.channel.send(help);
            message.delete();
        });
    }
}
exports.default = Help;
