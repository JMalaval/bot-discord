"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Random {
    static match(message) {
        return message.content.startsWith('!random');
    }
    static action(message) {
        const max = message.content.split(' ')[1] ? Number(message.content.split(' ')[1]) : 6;
        const random = Math.floor(Math.random() * Math.floor(max));
        message.delete();
        message.channel.send(random);
    }
}
exports.default = Random;
