"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const YoutubeStream = require('youtube-audio-stream');
class Play {
    static match(message) {
        return message.content.startsWith('!play');
    }
    static action(message) {
        let channelWanted = message.content.split(' ')[1];
        let urlWanted = message.content.split(' ')[2];
        let guildsChannel = message.guild.channels
            .filter((channel) => { return (channel.name === channelWanted && channel.type === 'voice'); })
            .first();
        message.delete();
        guildsChannel
            .join()
            .then((connection) => {
            const stream = YoutubeStream(urlWanted);
            connection.playStream(stream).on('end', () => {
                connection.disconnect();
            });
        })
            .catch((error) => console.warn(error));
    }
}
exports.default = Play;
