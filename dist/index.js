"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Discord = require("discord.js");
const help_1 = __importDefault(require("./command/help"));
const play_1 = __importDefault(require("./command/play"));
const random_1 = __importDefault(require("./command/random"));
const client = new Discord.Client();
client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});
client.on('message', (message) => {
    if (message.content.startsWith('!')) {
        let commands = message.content.split(' ')[0].replace('!', '');
        if (help_1.default.match(message))
            help_1.default.action(message);
        if (play_1.default.match(message))
            play_1.default.action(message);
        if (random_1.default.match(message))
            random_1.default.action(message);
        console.log("- Commande " + message.content.split(' ')[0] + " utilisée par " + message.author.username + " le " + new Date());
    }
});
client.on('messageUpdate', (oldMember, newMember) => {
    console.log('Presence:', newMember);
});
client.login('NTY1OTQxNzE1ODExMzAzNDI0.XK9xNQ.nDYRjmzgDGiJ-HSQUcbDFwCBdQM');
