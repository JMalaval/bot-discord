module.exports = {
    transform: {
        "^.+\\.ts$": "ts-jest"
    },
    moduleFileExtensions: [
        'js',
        'ts'
    ],
    testMach: [
        '**/test/**/*.test.ts'
    ],
    testEnvironment: 'node'
}